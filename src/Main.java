import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;
public class Main {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Scanner ler = new Scanner(System.in);
		
		//item
		 System.out.println("Insira n_Item:");
		 int i = ler.nextInt();
		 System.out.println("Insira nome");
		 String n = ler.next();
		 System.out.println("Insira valor");
		 double v = ler.nextDouble();
		 System.out.println("Insira data aquisição");
		 String d = ler.next();
		 itempatr item1 = new itempatr (i, n, v,  d);
		 
		//historico
		 System.out.println("Insira Historico:");
		 historico hist1 = new historico(item1, ler.next(), ler.next(), ler.next());
		 historico hist2 = new historico(item1, ler.next(), ler.next(), ler.next());
		 
		 System.out.println("Item:\n");
		 System.out.println(item1);
		 System.out.println("Historico:\n");
		 System.out.println(hist1);
		 System.out.println(hist2);
		 
		 //gravar no arquivo
		 FileWriter arq = new FileWriter("teste.txt");
		 PrintWriter gravarArq = new PrintWriter(arq);
		 
		 gravarArq.printf("Item:\n");
		 gravarArq.print(item1);
		 gravarArq.printf("Historico:\n");
		 gravarArq.print(hist1);
		 gravarArq.print(hist2);
		 arq.close();
	}

}
