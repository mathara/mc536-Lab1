public class itempatr {
	private int cod_prod;
	private String nome;
	private Double valor;
    private String dataDeLancamento;
    
    itempatr(int cod_prod , String nome, Double valor,String dataDeLancamento){
    	this.setCod_prod(cod_prod);
    	this.setNome(nome);
    	this.setValor(valor);
    	this.setDataDeLancamento(dataDeLancamento);
    }

	public int getCod_prod() {
		return cod_prod;
	}

	public void setCod_prod(int cod_prod) {
		this.cod_prod = cod_prod;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Double getValor() {
		return valor;
	}

	public void setValor(Double valor) {
		this.valor = valor;
	}

	public String getDataDeLancamento() {
		return dataDeLancamento;
	}

	public void setDataDeLancamento(String dataDeLancamento) {
		this.dataDeLancamento = dataDeLancamento;
	}
    
	//@Override
		public String toString(){
			String out = getNome()+ "(ID:" + getCod_prod()+")\n";
			out = out + " valor= " +getValor() + "\n";
			out = out + " data= " +getDataDeLancamento() + "\n";
			return out;
		}

    

}
