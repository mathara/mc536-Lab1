import java.sql.Date;

public class historico {
	private itempatr item;
	private String dataEntrada;
	private String dataSaida;
	private String local;
	
	historico(itempatr i, String E, String S, String l){
		this.setItem(i);
		this.setDataEntrada(E);
		this.setDataSaida(S);
		this.setLocal(l);
	}
	
	public itempatr getItem() {
		return item;
	}
	public void setItem(itempatr item) {
		this.item = item;
	}
	public String getDataEntrada() {
		return dataEntrada;
	}
	public void setDataEntrada(String dataEntrada) {
		this.dataEntrada = dataEntrada;
	}
	public String getDataSaida() {
		return dataSaida;
	}
	public void setDataSaida(String dataSaida) {
		this.dataSaida = dataSaida;
	}
	public String getLocal() {
		return local;
	}
	public void setLocal(String local) {
		this.local = local;
	}
	
	//@Override
	public String toString(){
		String out = (getItem()).getNome()+ "(ID:" + (getItem()).getCod_prod()+")\n";
		out = out + " data entrada= " +getDataEntrada() + "\n";
		out = out + " data saida= " +getDataSaida() + "\n";
		out = out + " local= " +getLocal() + "\n";
		return out;
	}

	
	

}
